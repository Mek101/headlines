#pragma once
#include "api_fwd.h"

namespace api
{
    class Comment
    {
    public:
        Comment(Json::Value jsonData, const std::string& kind);
        ~Comment();

        void AddChild(const CommentRef& child);
        bool IsValid();

        bool IsPlaceholder() const { return m_Kind == "more"; }
        const std::string& GetFullID() const { return m_FullID; }
        const std::string& GetText() const { return m_Text; }
        const std::string& GetSubreddit() const { return m_Subreddit; }
        const std::string& GetUserName() const { return m_UserName; }
        const std::vector<CommentRef>& GetChildren() const { return m_Children; }
        const std::string& GetPermaLink() const { return m_PermaLink; }
        const std::string& GetLinkID() const { return m_LinkID; }
        const std::string& GetID() const { return m_ID; }
        const std::string& GetKind() const { return m_Kind; }
        int GetScore() const { return m_Score; }
        bool IsModerator() const { return m_IsModerator; }
        bool IsOriginalPoster() const { return m_IsOriginalPoster; }
        bool IsUpVoted() const { return m_UpVoted; }
        bool IsDownVoted() const { return m_DownVoted; }
        bool IsBookmarked() const { return m_Bookmarked; }
        uint64_t GetTimeStamp() const { return m_Timestamp; }

        void UpVote();
        void RemoveUpVote();
        void DownVote();
        void RemoveDownVote();
        void Bookmark();
        void RemoveBookmark();

    private:
        void ParseChildComments(Json::Value replies);

        std::string m_UserName;
        std::string m_Text;
        int m_Score;
        std::vector<CommentRef> m_Children;
        std::string m_PermaLink;
        std::string m_LinkID;
        std::string m_Subreddit;
        bool m_IsOriginalPoster;
        bool m_IsModerator;
        bool m_UpVoted;
        bool m_DownVoted;
        bool m_Bookmarked;
        std::string m_ID;
        std::string m_FullID;
        std::string m_Kind;
        uint64_t m_Timestamp;
    };
}
