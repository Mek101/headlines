#pragma once

namespace api
{
    class IconCache
    {
    public:
        static IconCache* Get();

        bool IsUserIconCached(const std::string& userName);
        std::string GetUserIcon(const std::string& userName);
        void SetUserIcon(const std::string& userName, const std::string& iconPath);

        bool IsSubredditIconCached(const std::string& subredditName);
        std::string GetSubredditIcon(const std::string& subredditName);
        void SetSubredditIcon(const std::string& subredditName, const std::string& iconPath);
    private:
        void Load();
        void Save();
        std::string GetFilePath();

        static IconCache* s_Instance;

        std::mutex m_Mutex;
        std::unordered_map<std::string, std::string> m_UserIcons;
        std::unordered_map<std::string, std::string> m_SubredditIcons;
    };
}

