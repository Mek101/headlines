#include "Post.h"
#include "Application.h"
#include "util/Helpers.h"
#include "util/HtmlParser.h"
#include "UI/Pages/SearchPage.h"
#include "RedditAPI.h"

namespace api
{
    Post::Post(const Json::Value& postJson)
            : m_Title()
            , m_SelfText()
            , m_ImagePaths()
            , m_VideoPath()
            , m_SubredditPrefixed()
            , m_UserName()
            , m_UserNamePrefixed()
            , m_CommentsLink()
            , m_Url()
            , m_FullID()
            , m_ThumbHeight()
            , m_ThumbWidth()
            , m_Score()
            , m_NumComments()
            , m_IsVideo()
            , m_UpVoted(false)
            , m_DownVoted(false)
            , m_Bookmarked(false)
    {
        m_Title = util::Helpers::UnescapeHtmlForMarkdown(postJson["title"].asString());
        m_SelfText = postJson["selftext_html"].asString();
        std::string linkUrl = postJson["url"].asString();

        m_Subreddit = postJson["subreddit"].asString();
        {
            std::ostringstream oss;
            oss << "r/" << m_Subreddit;
            m_SubredditPrefixed = oss.str();
        }

        m_UserName = postJson["author"].asString();
        {
            std::ostringstream oss;
            oss << "u/" << m_UserName;
            m_UserNamePrefixed = oss.str();
        }

        m_Url = postJson["url"].asString();

        m_IsVideo = postJson["is_video"].asBool();
        if (m_IsVideo)
        {
            m_VideoPath = postJson["media"]["reddit_video"]["dash_url"].asString();
        }
        else if (m_Url.find("v.redd.it") != std::string::npos)
        {
            m_IsVideo = true;
            m_VideoPath = m_Url + "/DASHPlaylist.mpd";
        }
        else if (m_Url.find("youtube") != std::string::npos
            || m_Url.find("youtu.be") != std::string::npos)
        {
            m_IsVideo = true;
            m_VideoPath = m_Url;
        }

        m_ImagePaths.push_back(util::ImageCollection()); // higher index is better quality
        Json::Value previewImages = postJson["preview"]["images"][0]["resolutions"];
        for (unsigned int i = 0; i < previewImages.size(); ++i)
        {
            Json::Value previewImage = previewImages[i];
            util::ImageData imageData(previewImage["url"].asString(), previewImage["width"].asInt(), previewImage["height"].asInt());
            m_ImagePaths.back().m_Images.push_back(imageData);
        }

        if (!postJson["gallery_data"].isNull())
        {
            for (unsigned int i = 0; i < postJson["gallery_data"]["items"].size(); ++i)
            {
                const char* mediaId = postJson["gallery_data"]["items"][i]["media_id"].asCString();
                Json::Value images = postJson["media_metadata"][mediaId]["p"];

                m_ImagePaths.push_back(util::ImageCollection());
                for (unsigned int j = 0; j < images.size(); ++j)
                {
                    Json::Value image = images[j];
                    util::ImageData galleryImageData(image["u"].asString(), image["x"].asInt(), image["y"].asInt());
                    m_ImagePaths.back().m_Images.push_back(galleryImageData);
                }
            }
        }

        m_ThumbHeight = postJson["thumbnail_height"].asInt();
        m_ThumbWidth = postJson["thumbnail_width"].asInt();
        m_CommentsLink = postJson["permalink"].asString();
        m_Score = postJson["score"].asInt();
        m_NumComments = postJson["num_comments"].asInt();
        m_NSFW = postJson["over_18"].asBool();
        m_Spoiler = postJson["spoiler"].asBool();
        m_PostHint = postJson["post_hint"].asString();
        m_IsGallery = postJson["is_gallery"].asBool();

        m_FullID = postJson["name"].asString();

        std::string likes = postJson["likes"].asString();
        if (!likes.empty())
        {
            if (likes == "true")
            {
                m_UpVoted = true;
            }
            else
            {
                m_DownVoted = true;
            }
        }

        m_Bookmarked = postJson["saved"].asBool();

        unsigned int colour = util::Helpers::HexStringToUInt(postJson["link_flair_background_color"].asString());
        m_FlairColour = util::Colour(colour);
        m_FlairText = postJson["link_flair_text"].asString();
        m_FlairTextColour = postJson["link_flair_text_color"].asString() == "dark" ? util::Colour(0, 0, 0) : util::Colour(1, 1, 1);

        m_Timestamp = postJson["created_utc"].asUInt64();
    }

    void Post::UpVote()
    {
        if (m_DownVoted)
        {
            RemoveDownVote();
        }

        m_UpVoted = true;
        m_Score++;
        api::RedditAPI::Get()->Vote(m_FullID, 1);
    }

    void Post::RemoveUpVote()
    {
        m_UpVoted = false;
        m_Score--;
        api::RedditAPI::Get()->Vote(m_FullID, 0);
    }

    void Post::DownVote()
    {
        if (m_UpVoted)
        {
            RemoveUpVote();
        }

        m_DownVoted = true;
        m_Score--;

        api::RedditAPI::Get()->Vote(m_FullID, -1);
    }

    void Post::RemoveDownVote()
    {
        m_DownVoted = false;
        m_Score++;
        api::RedditAPI::Get()->Vote(m_FullID, 0);
    }

    void Post::Bookmark()
    {
        api::RedditAPI::Get()->Save(m_FullID);
        m_Bookmarked = true;
    }

    void Post::RemoveBookmark()
    {
        api::RedditAPI::Get()->UnSave(m_FullID);
        m_Bookmarked = true;
    }
}
