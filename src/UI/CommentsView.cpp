#include <iostream>
#include "AppSettings.h"
#include "CommentsView.h"
#include "Application.h"
#include "API/Post.h"
#include "API/RedditAPI.h"
#include "UI/Widgets/PostWidget.h"
#include "util/SimpleThread.h"
#include "util/Helpers.h"
#include "UI/Widgets/CommentUI.h"
#include "UI/Pages/CommentsPage.h"

namespace ui
{
    CommentsView::CommentsView(CommentsViewType type)
        : m_Type(type)
        , m_FailedToLoadWidget(nullptr)
        , m_Post(nullptr)
        , m_PostUI(nullptr)
        , m_CommentsDownloadThread(nullptr)
    {
        m_FailedToLoadDispatcher.connect([this]()
                                         {
                                             AddFailedToLoadUI();
                                         });
    }

    CommentsView::~CommentsView()
    {
        if (m_CommentsDownloadThread)
        {
            m_CommentsDownloadThread->Cancel();
        }
    }

    void CommentsView::CreateUI(Gtk::Viewport* viewport, Gtk::Box* parent)
    {
        m_Viewport = viewport;

        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/comments_view.ui");

        Gtk::Widget* clamp = builder->get_widget<Gtk::Widget>("Clamp");
        parent->append(*clamp);

        m_CommentsBox = builder->get_widget<Gtk::Box>("CommentsBox");
        if (m_Type == CommentsViewType::UserComments || m_Type == CommentsViewType::UserSavedComments)
        {
            m_CommentsBox->set_spacing(5);
        }

        m_PendingCommentsDispatcher.connect([this]()
                                         {
                                             AddCommentsToUI();
                                         });

        m_Viewport->get_vadjustment()->signal_value_changed().connect([this]()
                                                                      {
                                                                          OnViewportScrolled();
                                                                      });

        m_Spinner = builder->get_widget<Gtk::Spinner>("Spinner");
    }

    void CommentsView::Cleanup()
    {
        m_PostUI.reset();

        for (const CommentWidgetRef& commentUI : m_UIElements)
        {
            commentUI->OnParentCommentDestroyed();
        }
    }

    void CommentsView::LoadCommentsAsync(const std::string& url)
    {
        m_IsLoadingComments = true;
        m_Url = url;

        if (m_FailedToLoadWidget)
        {
            m_FailedToLoadWidget->set_visible(false);
        }

        if (!m_Spinner)
        {
            m_Spinner = new Gtk::Spinner();
            GetCommentBox()->append(*m_Spinner);
            m_Spinner->show();
            m_Spinner->set_size_request(-1, 100);
            m_Spinner->start();
        }

        m_CommentsDownloadThread = new util::SimpleThread([this](util::ThreadWorker* worker)
        {
            LoadComments(m_Url, worker);
        }, [this] (bool, util::SimpleThread*)
        {
            m_CommentsDownloadThread = nullptr;
        });
    }

    void CommentsView::LoadComments(const std::string& url, util::ThreadWorker* worker)
    {
        if (worker->IsCancelled())
        {
            return;
        }

        ::api::RedditAPI::Comments comments;
        switch (m_Type)
        {
            case CommentsViewType::PostComments:
            {
                if (worker->IsCancelled())
                {
                    return;
                }

                comments = ::api::RedditAPI::Get()->GetComments(Application::Get()->GetCommentsViewSorting(), url, AppSettings::Get()->GetInt("num_comments_to_load", 20));

                if (worker->IsCancelled())
                {
                    return;
                }

                if (comments.m_Comments.empty())
                {
                    m_FailedToLoadDispatcher.emit();
                    return;
                }
                break;
            }
            case CommentsViewType::PostSpecificComment:
            {
                if (worker->IsCancelled())
                {
                    return;
                }

                comments = ::api::RedditAPI::Get()->GetComments(Application::Get()->GetUserCommentsSorting(), url, AppSettings::Get()->GetInt("num_comments_to_load", 20), 2, 9, m_ContextComment);
                break;
            }
            case CommentsViewType::UserComments:
            {
                if (worker->IsCancelled())
                {
                    return;
                }

                std::string lastTimestamp = m_LastTimestamp;
                comments = ::api::RedditAPI::Get()->GetUserComments(m_User, Application::Get()->GetUserCommentsSorting(), AppSettings::Get()->GetInt("num_comments_to_load", 20), lastTimestamp);

                if (!worker->IsCancelled())
                {
                    m_LastTimestamp = lastTimestamp;
                }
                break;
            }
            case CommentsViewType::UserSavedComments:
            {
                if (worker->IsCancelled())
                {
                    return;
                }

                std::string lastTimestamp = m_LastTimestamp;
                comments = ::api::RedditAPI::Get()->GetUserSavedComments(AppSettings::Get()->GetInt("num_comments_to_load", 20), lastTimestamp);

                if (!worker->IsCancelled())
                {
                    m_LastTimestamp = lastTimestamp;
                }
                break;
            }
        }

        if (worker->IsCancelled())
        {
            return;
        }

        m_PendingCommentsMutex.lock();
        m_PendingComments = comments.m_Comments;
        m_CommentsToDownLoad = comments.m_CommentsToLoad;
        //reverse list so i can pop comments off the back when downloading.
        std::reverse(m_CommentsToDownLoad.begin(), m_CommentsToDownLoad.end());
        m_IsLoadingComments = false;
        m_PendingCommentsMutex.unlock();

        if (worker->IsCancelled())
        {
            return;
        }

        m_PendingCommentsDispatcher.emit();

    }

    void CommentsView::ClearComments(bool clearPost)
    {
        if (clearPost && m_PostUI)
        {
            m_CommentsBox->remove(*m_PostUI->GetWidget());
            delete m_PostUI->GetWidget();
            m_PostUI.reset();
        }

        for (const CommentWidgetRef& comment : m_UIElements)
        {
            m_CommentsBox->remove(*comment->GetBox());
        }

        m_PendingCommentsMutex.lock();
        m_PendingComments.clear();
        m_PendingCommentsMutex.unlock();
        m_Comments.clear();
        m_UIElements.clear();

        m_LastTimestamp = "";
    }

    void CommentsView::Reload()
    {
        ClearComments(false);
        LoadCommentsAsync(m_Url);
    }

    void CommentsView::AddCommentsToUI()
    {
        m_PendingCommentsMutex.lock();

        int numCommentsLoaded = 0;
        for (size_t i = 0; i < m_PendingComments.size(); ++i)
        {
            ::api::CommentRef comment = m_PendingComments[i];
            m_Comments.push_back(comment);
            m_UIElements.push_back(std::make_shared<CommentUI>(comment, m_Type, 0));
            m_UIElements.back()->CreateUI(m_CommentsBox);

            if (m_Type == CommentsViewType::UserComments || m_Type == CommentsViewType::UserSavedComments)
            {
                CommentWidgetRef commentWidget = m_UIElements.back();
                Glib::RefPtr<Gtk::GestureClick> gestureClick = Gtk::GestureClick::create();
                gestureClick->signal_released().connect([commentWidget] (int n_press, double, double)
                {
                    if (n_press <= 0)
                    {
                        return;
                    }

                    ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>();
                    Application::Get()->AddPage(commentView);

                    const api::PostRef& post = api::RedditAPI::Get()->GetPost(commentWidget->GetComment()->GetPermaLink());
                    commentView->SetPost(post, commentWidget->GetComment()->GetID());
                });

                commentWidget->GetBox()->add_controller(gestureClick);
                m_UIElements.back()->GetBox();
            }

            numCommentsLoaded++;
        }

        m_PendingComments.clear();
        m_PendingCommentsMutex.unlock();

        if (m_Spinner)
        {
            GetCommentBox()->remove(*m_Spinner);
            m_Spinner = nullptr;
        }
    }

    void CommentsView::SetPost(const api::PostRef& post, const std::string& contextComment)
    {
        GetCommentBox()->remove(*m_Spinner);
        m_Spinner = nullptr;

        m_Post = post;
        m_PostUI = std::make_shared<ui::PostWidget>(post, shared_from_this());
        m_PostUI->CreateUI(GetCommentBox());

        if (!contextComment.empty())
        {
            m_ContextComment = contextComment;
            m_Type = CommentsViewType::PostSpecificComment;
        }
    }

    void CommentsView::OnViewportScrolled()
    {
        if (!m_IsLoadingComments)
        {
            auto vAdjustment = m_Viewport->get_vadjustment();
            double distFromBottom = vAdjustment->get_upper() - vAdjustment->get_value();
            int viewportHeight = m_Viewport->get_height();
            if (distFromBottom == viewportHeight)
            {
                LoadMoreCommentsAsync();
            }
        }
    }

    void CommentsView::LoadMoreCommentsAsync()
    {
        m_IsLoadingComments = true;
        m_CommentsDownloadThread = new util::SimpleThread([this](util::ThreadWorker* worker)
        {
            LoadMoreComments(worker);
        }, [this] (bool, util::SimpleThread*)
        {
            m_CommentsDownloadThread = nullptr;
        });

        if (!m_Spinner)
        {
            m_Spinner = new Gtk::Spinner();
            GetCommentBox()->append(*m_Spinner);
            m_Spinner->show();
            m_Spinner->set_size_request(-1, 100);
            m_Spinner->start();
        }
    }

    void CommentsView::LoadMoreComments(util::ThreadWorker* worker)
    {
        if (worker->IsCancelled())
        {
            return;
        }

        if (m_Type == CommentsViewType::PostSpecificComment)
        {
            return;
        }

        std::vector<::api::CommentRef> newChildren;
        if (m_Type == CommentsViewType::PostComments)
        {
            std::vector<std::string> commentsToDownload;
            int numCommentsToLoad = std::min<int>(5, (int)m_CommentsToDownLoad.size());
            for (int i = 0; i < numCommentsToLoad; ++i)
            {
                commentsToDownload.push_back(m_CommentsToDownLoad.back());
                m_CommentsToDownLoad.pop_back();
            }

            if (worker->IsCancelled())
            {
                return;
            }

            newChildren = ::api::RedditAPI::Get()->GetMoreComments(m_Post->GetFullID(), -1, m_Post->GetFullID(), commentsToDownload);
        }
        else if (m_Type == CommentsViewType::UserSavedComments)
        {
            if (worker->IsCancelled())
            {
                return;
            }

            newChildren = ::api::RedditAPI::Get()->GetUserSavedComments(AppSettings::Get()->GetInt("num_comments_to_load", 20), m_LastTimestamp).m_Comments;
        }
        else
        {
            if (worker->IsCancelled())
            {
                return;
            }

            newChildren = ::api::RedditAPI::Get()->GetUserComments(m_User, Application::Get()->GetUserCommentsSorting(), AppSettings::Get()->GetInt("num_comments_to_load", 20), m_LastTimestamp).m_Comments;
        }

        if (worker->IsCancelled())
        {
            return;
        }

        m_PendingCommentsMutex.lock();

        for (const ::api::CommentRef& newComment : newChildren)
        {
            m_PendingComments.push_back(newComment);
        }

        m_IsLoadingComments = false;
        m_PendingCommentsMutex.unlock();

        if (worker->IsCancelled())
        {
            return;
        }
        m_PendingCommentsDispatcher.emit();
    }

    void CommentsView::InsertUserComment(const api::CommentRef& comment)
    {
        m_Comments.push_back(comment);
        CommentWidgetRef newCommentUI = std::make_shared<CommentUI>(comment, m_Type, 0);
        m_UIElements.push_back(newCommentUI);
        newCommentUI->CreateUI(m_CommentsBox, 1);
    }

    void CommentsView::SetUser(const api::UserRef& user)
    {
        m_User = user;
    }

    UISettings CommentsView::GetUISettings()
    {
        return UISettings
        {
            true,
            true,
            false,
            true,
            false,
            true,
            false,
            false
        };
    }

    void CommentsView::AddFailedToLoadUI()
    {
        if (m_FailedToLoadWidget)
        {
            m_FailedToLoadWidget->set_visible(true);
        }
        else
        {
            auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/error_loading.ui");
            m_FailedToLoadWidget = builder->get_widget<Gtk::Overlay>("Overlay");
            Gtk::Label* label = builder->get_widget<Gtk::Label>("Label");
            label->set_text("No comments found, retry?");
            Gtk::Button* button = builder->get_widget<Gtk::Button>("Button");
            button->signal_clicked().connect([this]()
                                             {
                                                 Reload();
                                             });
            m_CommentsBox->append(*m_FailedToLoadWidget);
        }

        if (m_Spinner)
        {
            m_CommentsBox->remove(*m_Spinner);
            delete m_Spinner;
            m_Spinner = nullptr;
        }
    }
}
