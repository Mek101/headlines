#pragma once
#include "RedditContentProvider.h"

namespace ui
{
    class PostWidget;
    class UserDownvotedPostsContentProvider : public RedditContentProvider
    {
    public:
        UserDownvotedPostsContentProvider();
    private:
        virtual std::vector<RedditContentListItemRef> GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker) override;
        virtual std::string GetFailedToLoadErrorMsg() override;
    };
}
