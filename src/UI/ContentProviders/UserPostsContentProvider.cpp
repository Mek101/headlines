#include <AppSettings.h>
#include "UserPostsContentProvider.h"
#include "UI/Widgets/PostWidget.h"
#include "API/RedditAPI.h"
#include "Application.h"
#include "util/ThreadWorker.h"

namespace ui
{
    UserPostsContentProvider::UserPostsContentProvider()
    {

    }
    
    void UserPostsContentProvider::SetUser(const api::UserRef& user)
    {
        m_User = user;
    }

    std::vector<RedditContentListItemRef> UserPostsContentProvider::GetContentInternal(std::string& lastTimeStamp, util::ThreadWorker* worker)
    {
        std::vector<api::PostRef> posts = api::RedditAPI::Get()->GetUserPosts(m_User, Application::Get()->GetUserPostsSorting(), AppSettings::Get()->GetInt("num_posts_to_load", 5), lastTimeStamp);
        std::vector<RedditContentListItemRef> uiElements;
        for (const api::PostRef& post : posts)
        {
            if (worker->IsCancelled())
            {
                return uiElements;
            }

            PostWidgetRef postUI = std::make_shared<PostWidget>(post);
            uiElements.push_back(postUI);
        }

        return uiElements;
    }

    std::string UserPostsContentProvider::GetFailedToLoadErrorMsg()
    {
        return "No posts found. Retry?";
    }
}