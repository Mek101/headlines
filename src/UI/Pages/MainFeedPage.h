#pragma once

#include "Page.h"

namespace ui
{
    class MainFeedPage : public Page
    {
    public:
        MainFeedPage();
        ~MainFeedPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::Posts; }
        virtual UISettings GetUISettings() const override;
    private:

        ui::RedditContentListViewRef m_ListView;
        int m_LoginStatusChangedHandler;
    };
}