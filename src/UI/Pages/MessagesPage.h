#pragma once

#include "UI/Pages/Page.h"

namespace ui
{
    class MessagesPage : public Page
    {
    public:
        explicit MessagesPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void OnHeaderBarCreated() override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override;
    private:
        ui::MessagesContentProviderRef m_InboxContentProvider;
        ui::RedditContentListViewRef m_InboxListView;

        ui::MessagesContentProviderRef m_SentContentProvider;
        ui::RedditContentListViewRef m_SentListView;

        AdwViewStack* m_Stack = nullptr;
        AdwViewSwitcherBar* m_ViewSwitcherBar = nullptr;
    };
}
