#include <Application.h>
#include "SavedPage.h"
#include "AppSettings.h"
#include "UI/Widgets/HeaderBar.h"
#include "UI/ListViews/RedditContentListView.h"
#include "UI/ContentProviders/UserSavedPostsContentProvider.h"

namespace ui
{
    SavedPage::SavedPage()
        : Page(PageType::SavedPage)
        , m_CommentsView(CommentsViewType::UserSavedComments)
        , m_SavedPostsView(std::make_shared<RedditContentListView>(std::make_shared<UserSavedPostsContentProvider>()))
    {

    }

    void SavedPage::Cleanup()
    {

    }

    void SavedPage::Reload()
    {
        ReloadPosts();
        ReloadComments();
    }

    Gtk::Box*  SavedPage::CreateUIInternal(AdwLeaflet* parent)
    {
        auto builder = Gtk::Builder::create_from_resource("/io/gitlab/caveman250/headlines/ui/saved.ui");

        Gtk::Box* box = builder->get_widget<Gtk::Box>("SavedViewBox");
        m_LeafletPage = adw_leaflet_append(parent, (GtkWidget*)box->gobj());
        adw_leaflet_page_set_name(m_LeafletPage, GetID().c_str());

        m_Stack = (AdwViewStack*)builder->get_widget<Gtk::Widget>("Notebook")->gobj();
        m_SwitcherBar = (AdwViewSwitcherBar *)builder->get_widget<Gtk::Widget>("switcher_bar")->gobj();

        Gtk::Viewport* postsViewport = builder->get_widget<Gtk::Viewport>("PostsViewport");
        Gtk::Box* postsBox = builder->get_widget<Gtk::Box>("PostsBox");
        m_SavedPostsView->CreateUI(postsBox, postsViewport);
        m_SavedPostsView->LoadContentAsync();

        Gtk::Viewport* commentsViewport = builder->get_widget<Gtk::Viewport>("CommentsViewport");
        Gtk::Box* commentsBox = builder->get_widget<Gtk::Box>("CommentsBox");
        m_CommentsView.CreateUI(commentsViewport, commentsBox);
        m_CommentsView.LoadCommentsAsync("");

        return box;
    }

    void SavedPage::OnHeaderBarCreated()
    {
        AdwViewSwitcherTitle* viewSwitcher = GetHeaderBar()->AddViewSwitcher(m_Stack);
        g_object_bind_property(viewSwitcher, "title-visible", m_SwitcherBar, "reveal", GBindingFlags::G_BINDING_SYNC_CREATE);
        GetHeaderBar()->SetTitle("Saved");
    }

    void SavedPage::ReloadPosts()
    {
        m_SavedPostsView->ClearContent();
        m_SavedPostsView->LoadContentAsync();
    }

    void SavedPage::ReloadComments()
    {
        m_CommentsView.ClearComments();
        m_CommentsView.LoadCommentsAsync("");
    }

    SortType SavedPage::GetSortType() const
    {
        return SortType::None;
    }

    UISettings SavedPage::GetUISettings() const
    {
        return UISettings
        {
            true,
            false,
            false,
            true,
            false,
            true,
            false,
            false
        };
    }
}