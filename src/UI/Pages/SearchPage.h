#pragma once

#include "Page.h"
#include "API/Subreddit.h"
#include "API/Post.h"
#include "API/User.h"

namespace ui
{
    class SearchPage : public Page
    {
    public:
        SearchPage();
        Gtk::Box* CreateUIInternal(AdwLeaflet* parent) override;
        virtual void Cleanup() override;
        virtual void Reload() override;
        virtual SortType GetSortType() const override { return SortType::None; }
        virtual UISettings GetUISettings() const override;

        void SetSearchString(const std::string& string);
        std::string GetSearchString() const { return m_SearchString; }
    private:
        virtual void OnHeaderBarCreated() override;

        ui::UserSearchContentProviderRef m_UserSearchProvider;
        ui::SubredditSearchContentProviderRef m_SubredditSearchProvider;
        ui::RedditContentListViewRef m_PostsView;
        ui::RedditContentListBoxViewRef m_SubredditView;
        ui::RedditContentListBoxViewRef m_UsersView;
        std::string m_SearchString;

        AdwViewStack* m_Stack = nullptr;
        AdwViewSwitcherBar* m_ViewSwitcherBar = nullptr;
    };
}