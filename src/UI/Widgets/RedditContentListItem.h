#pragma once

namespace ui
{
    class RedditContentListItem
    {
    public:
        RedditContentListItem() : m_Active(true) {}
        virtual ~RedditContentListItem() {}
        virtual void CreateUI(Gtk::Widget* parent) = 0;
        virtual void RemoveUI() = 0;
        virtual void OnActivate() {}

        virtual int GetContentTop() = 0;
        virtual int GetContentBottom() = 0;
        virtual void SetActive(bool active) { m_Active = active; }

        bool GetActive() const { return m_Active; }
        virtual Gtk::Widget* GetWidget() = 0;

    protected:
        bool m_Active;
    };
}
