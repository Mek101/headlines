#pragma once

#ifdef WIN32
#undef WIN32
#include <gtkmm.h>
#include <adwaita.h>
#define WIN32 1
#else
#include <gtkmm.h>
#include <adwaita.h>
#endif
#include <json/json.h>
#include <string>
#include <iostream>
#include <filesystem>
#include <thread>
#include <mutex>
#include <fstream>
#include <functional>
#include <chrono>
#include <libxml/tree.h>
#include "util/ImageData.h"
#include "util/Helpers.h"

#if !DIST_BUILD
extern bool IsDebuggerAttached();
#if WIN32
#define DEBUG_BREAK() if (IsDebuggerAttached()) { __debugbreak(); }
#else
#define DEBUG_BREAK() raise(SIGINT);
#endif
#else
#define DEBUG_BREAK()
#endif

#define CAT_III(_, expr) expr
#define CAT_II(a, b) CAT_III(~, a ## b)
#define CAT(a, b) CAT_II(a, b)

#define VARG_COUNT_01N(...) EXPAND_ARGS(AUGMENT_ARGS(__VA_ARGS__), VARG_COUNT_01N_HELPER())

#if !WIN32 //wat, TODO
#define VARG_COUNT_01N_HELPER() N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, 1, 0
#else
#define VARG_COUNT_01N_HELPER() N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, 1, 0
#endif

#define EXPAND(x) x

#define AUGMENT_ARGS(...) unused, __VA_ARGS__
#define EXPAND_ARGS(...) EXPAND(GETARGCOUNT(__VA_ARGS__))
#define GETARGCOUNT(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, count, ...) count


#define ASSERT_MESSAGE_0(msg_buf, ...) snprintf(msg_buf, 1024, "");
#define ASSERT_MESSAGE_1(msg_buf, ...) snprintf(msg_buf, 1024, __VA_ARGS__);
#define ASSERT_MESSAGE_N(msg_buf, msg, ...) snprintf(msg_buf, 1024, msg, __VA_ARGS__);
#define ASSERT_MESSAGE(msg_buf, ...) CAT(ASSERT_MESSAGE_, VARG_COUNT_01N(__VA_ARGS__))(msg_buf, __VA_ARGS__)

#define ASSERT(expr, ...) \
	do { \
		if (!(expr))\
		{\
			char msg_buf[1024]; \
			ASSERT_MESSAGE(msg_buf, __VA_ARGS__)\
			char buf[1024]; \
			snprintf(buf, 1024, "Expression: %s\n\nMessage: %s\n", #expr, (const char*)&msg_buf);			\
 			g_critical("Assertion failed:\n%s - at %s:%i\n\n", (const char*)&buf, __FILE__, __LINE__); \
 			DEBUG_BREAK() \
		}\
	} while (0)

#define VERIFY(expr, ...) (!(expr) ? (::std::invoke([&](bool result) -> bool  { ASSERT(expr, __VA_ARGS__); return result; }, !!(expr))), false : true)


