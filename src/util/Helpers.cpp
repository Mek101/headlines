#include <sstream>
#include "Helpers.h"
#include "Application.h"
#include "API/RedditAPI.h"
#include "UI/Pages/CommentsPage.h"
#include "UI/Pages/SubredditPage.h"
#include "UI/Pages/UserPage.h"
#include "SimpleThread.h"

#if WIN32
#include <windows.h>
#endif

namespace util
{
    void Helpers::SplitString(const std::string& str, std::vector<std::string>& cont, char delim)
    {
        std::stringstream ss(str);
        std::string token;
        while (std::getline(ss, token, delim))
        {
            cont.push_back(token);
        }
    }

    Json::Value Helpers::GetJsonValueFromString(const std::string& str)
    {
        Json::CharReaderBuilder builder;
        builder["collectComments"] = false;
        Json::Value jsonValue;
        JSONCPP_STRING errs;
        Json::CharReader* charReader = builder.newCharReader();

        if (!charReader->parse(str.data(), str.data() + str.size(), &jsonValue, &errs))
        {
            g_warning("Json::CharReader error: %s", errs.c_str());
        }

        delete charReader;
        return jsonValue;
    }

    void Helpers::OpenBrowser(const std::string& url)
    {
        std::ostringstream oss;
#if WIN32
        std::string sanitisedUrl = url;
        auto it = sanitisedUrl.find('&');
        while (it != std::string::npos)
        {
            sanitisedUrl.replace(it, 1, "^&");
            it = sanitisedUrl.find('&', it + 2);
        }
        oss << "start " << sanitisedUrl;
#else
        oss << "xdg-open \"" << url << "\"";
#endif
        int ret = system(oss.str().c_str());
        if (ret != 0)
        {
            g_warning("Error: %i while running xdg-open", ret);
        }
    }

    std::string Helpers::UnescapeHtml(const std::string& data)
    {
        std::string ret = data;

        auto it = ret.find("&quot;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 6, "\"");
            it = ret.find("&quot;");
        }

        it = ret.find("&amp;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 5, "&");
            it = ret.find("&amp;");
        }

        it = ret.find("&lt;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 4, "<");
            it = ret.find("&lt;");
        }

        it = ret.find("&gt;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 4, ">");
            it = ret.find("&gt;");
        }

        return ret;
    }

    std::string Helpers::UnescapeHtmlForMarkdown(const std::string& data)
    {
        std::string ret = data;

        auto it = ret.find("&quot;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 6, "\"");
            it = ret.find("&quot;");
        }

        //gtk markup requires that this is still escaped.
//        it = ret.find("&amp;");
//        while(it != std::string::npos)
//        {
//            ret = ret.replace(it, 5, "&");
//            it = ret.find("&amp;");
//        }

        it = ret.find("&lt;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 4, "<");
            it = ret.find("&lt;");
        }

        it = ret.find("&gt;");
        while(it != std::string::npos)
        {
            ret = ret.replace(it, 4, ">");
            it = ret.find("&gt;");
        }

        return ret;
    }

    uint64_t Helpers::GetSecondsSinceEpoch()
    {
        const auto now = std::chrono::system_clock::now();
        const auto epoch = now.time_since_epoch();
        const auto seconds = std::chrono::duration_cast<std::chrono::seconds>(epoch);
        return seconds.count();
    }

    std::string Helpers::TimeStampToDateString(uint64_t timeStamp)
    {
        std::time_t temp = timeStamp;
        std::tm* t = std::localtime(&temp);
        std::stringstream ss;
        ss << std::put_time(t, "%d/%m/%Y");
        return ss.str();
    }

    std::string Helpers::TimeStampToTimeAndDateString(uint64_t timeStamp)
    {
        std::time_t temp = timeStamp;
        std::tm* t = std::localtime(&temp);
        std::stringstream ss;
        ss << std::put_time(t, "%H:%M - %d/%m/%Y");
        return ss.str();
    }

    void Helpers::CopyToClipboard(const std::string& str)
    {
        Glib::RefPtr<Gdk::Clipboard> clipboard = Gdk::Display::get_default()->get_clipboard();
        if (!clipboard)
        {
            return;
        }

        clipboard->set_text(str);
    }

    xmlNodePtr Helpers::FindXmlNodeByName(xmlNodePtr rootNode, const xmlChar* nodeName)
    {
        xmlNodePtr node = rootNode;
        if (node == nullptr)
        {
            return nullptr;
        }

        while (node != nullptr)
        {
            if (!xmlStrcmp(node->name, nodeName))
            {
                return node;
            }
            else if (node->children != nullptr)
            {
                xmlNodePtr intNode = FindXmlNodeByName(node->children, nodeName);
                if (intNode != nullptr)
                {
                    return intNode;
                }
            }
            node = node->next;
        }
        return nullptr;
    }

    void Helpers::HandleLink(std::string link)
    {
        if (link.find('/') == 0)
        {
            link.erase(0, 1);
        }

        bool isRedditLink = link.find("u/") == 0 || link.find("r/") == 0;
        size_t redditcomPos = link.find("reddit.com");
        bool isRedditUrl =  redditcomPos != std::string::npos;
        if (isRedditLink || isRedditUrl)
        {
            if (isRedditUrl)
            {
                //get start of path
                size_t it = redditcomPos + strlen("reddit.com/");
                link.erase(0, it);
            }

            std::vector<std::string> split = util::Helpers::SplitString(link, "/");
            if (split[0] == "u")
            {
                ui::UserPageRef userView = std::make_shared<ui::UserPage>(split[1]);
                Application::Get()->AddPage(userView);
                return;
            }
            else if (split[0] == "r")
            {
                if (split.size() > 1)
                {
                    if (split.size() > 2)
                    {
                        if (split[2] == "comments")
                        {
                            //check if we need context
                            std::unordered_map<std::string, std::string> args = GetUrlArgs(link);
                            std::string contextComment;
                            if (args.count("context") > 0)
                            {
                                contextComment = split[split.size() - 2];
                            }

                            ui::CommentsPageRef commentView = std::make_shared<ui::CommentsPage>(link, contextComment);
                            Application::Get()->AddPage(commentView);
                            return;
                        }

                        ASSERT(false, "Unhandled reddit link: %s", link.c_str());
                    }
                    else
                    {
                        ui::SubredditPageRef subView = std::make_shared<ui::SubredditPage>(split[1]);
                        Application::Get()->AddPage(subView);
                        return;
                    }
                }
            }
        }

        Application::Get()->ShowNotification("Opening link...");
        std::stringstream ss;
        ss << "xdg-open " << link;
        int ret = system(ss.str().c_str());
        if (ret != 0)
        {
            g_warning("Error: %i while opening link", ret);
        }
    }

    void Helpers::MSleep(int milliseconds)
    {
        struct timespec ts{};

        ts.tv_sec = milliseconds / 1000;
        ts.tv_nsec = (milliseconds % 1000) * 1000000;

        nanosleep(&ts, &ts);
    }

    unsigned int Helpers::HexStringToUInt(std::string hex)
    {
        unsigned int ret;

        hex.erase(0, 1); //remove the #

        std::stringstream ss;
        ss << std::hex << hex;
        ss >> ret;

        return ret;
    }

    std::vector<std::string> Helpers::SplitString(const std::string& str, const std::string& delim)
    {
        std::vector<std::string> ret;
        std::string input = str;

        size_t pos;
        while ((pos = input.find(delim)) != std::string::npos)
        {
            ret.push_back(input.substr(0, pos));
            input.erase(0,pos + delim.length());
        }

        if (!input.empty())
        {
            ret.push_back(input);
        }
        return ret;
    }

    std::string Helpers::FormatString(const std::string& fmt, ...)
    {
        char buf[256];

        va_list args;
        va_start(args, fmt);
        const auto r = std::vsnprintf(buf, sizeof buf, fmt.c_str(), args);
        va_end(args);

        if (r < 0)
        {
            g_warning("failed to format string.");
            return std::string();
        }

        const size_t len = r;
        if (len < sizeof buf)
        {
            return { buf, len };
        }

        std::string s(len, '\0');
        va_start(args, fmt);
        std::vsnprintf(s.data(), len + 1, fmt.c_str(), args);
        va_end(args);

        return s;
    }

    void Helpers::ApplyCardStyle(Gtk::Widget* widget, bool rounded)
    {
        if (!widget->get_style_context()->has_class("card"))
        {
            widget->get_style_context()->add_class("card");
        }

        if (!rounded && !widget->get_style_context()->has_class("no_rounded_corners"))
        {
            widget->get_style_context()->add_class("no_rounded_corners");
        }
        else if (rounded && widget->get_style_context()->has_class("no_rounded_corners"))
        {
            widget->get_style_context()->remove_class("no_rounded_corners");
        }
    }

    std::string Helpers::GetFileExtension(const std::string& filePath)
    {
        size_t it = 0;
        size_t extensionIndex = 0;
        while (it != std::string::npos)
        {
            extensionIndex = it + 1;
            it = filePath.find('.', it + 1);
        }

        std::string fileExtension = filePath.substr(extensionIndex, filePath.size() - extensionIndex);

        return fileExtension;
    }

    std::string Helpers::GetMimeType(const std::string& filePath)
    {
        static std::unordered_map<std::string, std::string> mimeTypeMap =
        {
                { "png",  "image/png" },
                { "mov",  "video/quicktime" },
                { "mp4",  "video/mp4" },
                { "jpg",  "image/jpeg" },
                { "jpeg", "image/jpeg" },
                { "gif",  "image/gif" },
        };

        std::string extension = GetFileExtension(filePath);
        ASSERT(mimeTypeMap.find(extension) != mimeTypeMap.end(), "Helpers::GetMimeType - Unhandled file type %s", extension.c_str());
        return mimeTypeMap[extension];
    }

    std::string Helpers::GetFileName(const std::string& filePath)
    {
        size_t it = 0;
        size_t slashIndex = 0;
        while (it != std::string::npos)
        {
            slashIndex = it + 1;
            it = filePath.find('/', it + 1);
        }

        std::string fileExtension = filePath.substr(slashIndex, filePath.size() - slashIndex);

        return fileExtension;
    }

    std::string Helpers::StringReplace(std::string str, const std::string& find, const std::string& replace)
    {
        size_t it = str.find(find);
        while (it != std::string::npos)
        {
            str.replace(it, find.length(), replace);
            it = str.find(find);
        }

        return str;
    }

    std::unordered_map<std::string, std::string> Helpers::GetUrlArgs(const std::string& url)
    {
        std::unordered_map<std::string, std::string> args;

        size_t argsIndex = url.find('?');
        std::string argsString = argsIndex != std::string::npos ? url.substr(argsIndex + 1, url.size() - argsIndex + 1) : "";
        if (!argsString.empty())
        {
            std::vector<std::string> argsSplit;
            util::Helpers::SplitString(argsString, argsSplit, '&');

            for (const auto& arg: argsSplit)
            {
                std::vector<std::string> kvpSplit;
                util::Helpers::SplitString(arg, kvpSplit, '=');
                if (VERIFY(kvpSplit.size() == 2, "Helpers::HandleLink - Badly formed argument."))
                {
                    args[kvpSplit[0]] = kvpSplit[1];
                }
            }
        }

        return args;
    }
}
