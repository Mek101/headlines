#pragma once

#include <libxml/HTMLparser.h>

namespace util
{
    class HtmlParser
    {
    public:
        enum class BlockType
        {
            None,
            Text,
            Code,
            Quote,
            Table,
        };

        enum class NodeType
        {
            None,
            Text,
            Code,
            Quote,
            Table,
            TableRow,
            TableHeader,
            TableData,
            TableHeadScope,
            TableBodyScope,
            TableFootScope,
            Ul,
            Li,
            Hr,
            Br,
            Pre,
        };

        class Block
        {
        public:
            BlockType m_Type;
            bool m_ContainsSpoilers = false;
        };

        class TextBlock : public Block
        {
        public:
            std::stringstream m_StringStream;
        };

        struct TableCell
        {
            bool m_IsHeader;
            std::stringstream m_StringStream;
        };

        class TableBlock : public Block
        {
        public:
            std::vector<std::vector<TableCell>> m_TableValues;
        };

        HtmlParser();

        void ParseHtml(const std::string& html, Gtk::Box* parent, int maxLength, bool hideSpoilers);

    private:
        void ParseElement(xmlNode* node, bool hideSpoilers);
        void ParseNode(xmlNode* node, bool hideSpoilers);
        void ParseText(xmlNode* node, bool spoiler, bool hideSpoilers);
        void ParseTableText(xmlNode* node, bool spoiler, bool hideSpoilers);
        static void EscapeText(std::string& string);

        std::vector<std::shared_ptr<Block>> m_TextBlocks;
        NodeType m_ParentNodeType;
        int m_TextLength;
        int m_MaxTextLength;
    };
}
