#include "ImageDownloader.h"
#include "Application.h"
#include "AppSettings.h"
#include "SimpleThread.h"
#include "ThreadWorker.h"
#include "Helpers.h"

namespace util
{
    void ImageDownloader::DownloadImageAsync(const util::ImageData& imageData, std::function<void(const std::string&)> onFinished)
    {
        if (m_ImageDownloadThread)
        {
            g_warning("Attempting to download an image with an active download thread. aborting.");
            return;
        }

        m_ImageData = imageData;
        m_OnFinished = onFinished;

        if (m_ImageData.m_Url.empty())
        {
           return;
        }

        m_ImageDownloadThread = new util::SimpleThread([this](util::ThreadWorker* worker)
        {
            DownloadImageInternal(worker);
        }, [this](bool cancelled, util::SimpleThread*)
        {
            if (!cancelled)
            {
                m_ImageDownloadThread = nullptr;

                if (m_OnFinished)
                {
                    m_OnFinished(m_ImageData.GetFSPath());
                }
            }
        });
    }

    void ImageDownloader::DownloadImageInternal(util::ThreadWorker* worker)
    {
        if (worker->IsCancelled())
        {
            return;
        }

        std::string filePath = m_ImageData.GetFSPath();

        if (worker->IsCancelled())
        {
            return;
        }

        if (!std::filesystem::exists(filePath))
        {
            Application::Get()->GetHttpClient().DownloadImage(m_ImageData.m_Url, filePath, { });
        }

        if (worker->IsCancelled())
        {
            return;
        }
    }

    void ImageDownloader::Cancel()
    {
        if (m_ImageDownloadThread)
        {
            m_ImageDownloadThread->Cancel();
            m_ImageDownloadThread = nullptr;
        }
    }
}
