#include <pch.h>
#include "PasswordManager.h"

namespace util
{
    static void OnPasswordStored(GObject*, GAsyncResult* result, gpointer)
    {
        GError* error = NULL;
        secret_password_store_finish(result, &error);

        if (error != NULL)
        {
            g_warning("PasswordManager - Error storing password: %s", error->message);
            g_error_free(error);
        }
    }

    static const SecretSchema* GetSchema()
    {
        static const SecretSchema schema = {
                "caveman250.gtkeddit.Password", SECRET_SCHEMA_NONE,
                {
                        {  "type", SECRET_SCHEMA_ATTRIBUTE_STRING },
                        { "NULL", SECRET_SCHEMA_ATTRIBUTE_STRING },
                    },
                0, NULL, NULL, NULL, NULL, NULL, NULL, NULL
        };

        return &schema;
    }

    void PasswordManager::SaveToken(const std::string& token)
    {
        secret_password_store(GetSchema(), SECRET_COLLECTION_DEFAULT, "Gtkeddit - Token",
                              token.c_str(), NULL, OnPasswordStored, NULL,
                              "type", "token",
                              (char*)NULL);
    }

    void PasswordManager::SaveRefreshToken(const std::string& refreshToken)
    {
        secret_password_store(GetSchema(), SECRET_COLLECTION_DEFAULT, "Gtkeddit - Refresh Token",
                              refreshToken.c_str(), NULL, OnPasswordStored, NULL,
                              "type", "refresh_token",
                              (char*)NULL);
    }

    void PasswordManager::SaveTokenExpiry(uint64_t expiry)
    {
        std::ostringstream oss;
        oss << expiry;

        secret_password_store(GetSchema(), SECRET_COLLECTION_DEFAULT, "Gtkeddit - Token Expire",
                              oss.str().c_str(), NULL, OnPasswordStored, NULL,
                              "type", "token_expiry",
                              (char*)NULL);
    }

    std::string PasswordManager::LoadToken()
    {
        GError* error = NULL;
        gchar *password = secret_password_lookup_sync(GetSchema(), NULL, &error, "type", "token", (char*)NULL);

        if (error != NULL)
        {
            g_warning("PasswordManager - Error loading token: %s", error->message);
            g_error_free(error);
        }

        if (password == nullptr)
        {
            return "";
        }

        return password;
    }

    std::string PasswordManager::LoadRefreshToken()
    {
        GError* error = NULL;
        gchar *password = secret_password_lookup_sync(GetSchema(), NULL, &error, "type", "refresh_token", (char*)NULL);

        if (error != NULL)
        {
            g_warning("PasswordManager - Error loading refresh token %s", error->message);
            g_error_free(error);
        }

        if (password == nullptr)
        {
            return "";
        }

        return password;
    }

    uint64_t PasswordManager::LoadTokenExpiry()
    {
        GError* error = NULL;
        gchar* password = secret_password_lookup_sync(GetSchema(), NULL, &error, "type", "token_expiry", (char*)NULL);

        if (error != NULL)
        {
            g_warning("PasswordManager - Error loading token expiry: %s", error->message);
            g_error_free(error);
        }

        if (password == nullptr)
        {
            return 0;
        }

        uint64_t ret;
        std::istringstream iss(password);
        iss >> ret;

        return ret;
    }

#define CHECK_ERROR() \
if (!VERIFY(error == nullptr, "%s", error->message))\
{\
    g_error_free(error);\
    error = nullptr;\
}

    static SecretCollection* GetSecretCollection(SecretService* service)
    {
        GError* error = nullptr;
        SecretCollection* collection = secret_collection_for_alias_sync(service, "default", SECRET_COLLECTION_LOAD_ITEMS, nullptr, &error);
        CHECK_ERROR()

        return collection;
    }

    bool PasswordManager::EnsureCollectionUnlocked()
    {
        GError* error = nullptr;
        SecretService* service = secret_service_get_sync(SECRET_SERVICE_LOAD_COLLECTIONS, nullptr, &error);
        CHECK_ERROR()

        SecretCollection* collection = GetSecretCollection(service);

        if (!VERIFY(collection, "Could not find default keyring."))
        {
            return false;
        }

        if (secret_collection_get_locked(collection))
        {
            error = nullptr;
            GList* unlocked = nullptr;
            GList* list = g_list_alloc();
            list->data = collection;
            secret_service_unlock_sync(service, list, nullptr, &unlocked, &error);
            CHECK_ERROR()
            g_list_free(list);

            if (VERIFY(unlocked, "Failed to unlock default keyring"))
            {
                SecretCollection* unlockedCollection =  (SecretCollection*)unlocked->data;
                CHECK_ERROR()
                g_list_free(unlocked);
                return unlockedCollection;
            }

            return false;
        }
        else
        {
            return true;
        }
    }
}