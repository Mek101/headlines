#pragma once

#include "pch.h"

namespace util
{
    class ThreadWorker
    {
    public:
        ThreadWorker();

        void Run(const std::function<void(ThreadWorker*)>& task, const std::function<void(bool, util::ThreadWorker*)>& onFinished);

        void Cancel();

        bool IsCancelled() const;

    private:
        mutable std::mutex m_Mutex;

        bool m_Cancelled;
        bool m_Finished;
    };
}